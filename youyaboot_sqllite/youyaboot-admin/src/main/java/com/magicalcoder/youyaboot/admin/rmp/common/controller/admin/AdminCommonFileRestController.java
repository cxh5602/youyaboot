package com.magicalcoder.youyaboot.admin.rmp.common.controller.admin;

import com.magicalcoder.youyaboot.admin.common.config.UploadFilePathConfig;
import com.magicalcoder.youyaboot.core.common.exception.BusinessException;
import com.magicalcoder.youyaboot.core.serialize.CommonReturnCode;
import com.magicalcoder.youyaboot.core.serialize.ResponseMsg;
import com.magicalcoder.youyaboot.core.utils.MapUtil;
import com.magicalcoder.youyaboot.core.utils.StringUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by magicalcoder.com on 2015/9/8.
 * 799374340@qq.com
 */
@RestController
@RequestMapping(value="/admin/common_file_rest/")
public class AdminCommonFileRestController {

    @Resource
    private UploadFilePathConfig uploadFilePathConfig;

    private String originFilePath(String originFile){
        if(StringUtil.isBlank(originFile) || originFile.startsWith("http://") || originFile.startsWith("https://")){
           return "";
        }
        String[] arr = originFile.split("/");
        StringBuilder pathBuilder = new StringBuilder();
        for(int i=0;i<arr.length;i++){
            if(i!=arr.length-1){
                pathBuilder.append(arr[i]).append("/");
            }
        }

        String path = pathBuilder.toString();
        String extraPrefix = uploadFilePathConfig.getFileExtraAddPrefix();

        String prefix = extraPrefix;
        if(StringUtil.isNotBlank(prefix)){
            if(path.startsWith(prefix)){
                path = path.substring(prefix.length());//把虚拟前缀都去掉
            }
        }
        if("/".equals(path)){
            path = "";
        }
        return path;
    }

    @RequestMapping(value = "upload",method = RequestMethod.POST)
    public ResponseMsg fileUpload(@RequestParam MultipartFile[] file,
                                  @RequestParam(required = false,value = "originFile") String originFile,
                                  HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String originFilePath = originFilePath(originFile);
        String realPath = uploadFilePathConfig.getUploadDiskFolder()+ originFilePath ;
        //如果文件夹不存在就新建一个文件夹 聪明的根据当前目录规则来进行上传
        File dirPath = new File(realPath);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
        String originalFilename = null;
        for (MultipartFile myfile : file) {
            if (!myfile.isEmpty()) {
                // 获取文件名
                originalFilename = myfile.getOriginalFilename();
                // 文件名后缀处理
                String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));

                SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                String newFileName = df.format(new Timestamp(System.currentTimeMillis())) + "_"
                        + (int) (Math.random() * 900000 + 100000) + suffix;
                File storeFile = new File(realPath, newFileName);
                if(!storeFile.getParentFile().exists()){
                    storeFile.getParentFile().mkdirs();
                }
                FileUtils.copyInputStreamToFile(
                        myfile.getInputStream(), storeFile);//上传文件到磁盘
                String prefix = uploadFilePathConfig.getFileExtraAddPrefix() +originFilePath;
                String src = prefix+newFileName;
                while (src.contains("//")){
                    src = src.replace("//","/");
                }
                return new ResponseMsg(MapUtil.buildMap("src",src,"title",originalFilename));
            }
        }
        throw new BusinessException(CommonReturnCode.FILE_UPLOAD_NO_FILE);
    }


}
